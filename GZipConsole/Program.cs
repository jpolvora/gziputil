﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace GZipConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            //var zipTeste = new[] { "zip", ".", "teste.gz" };

            //var unzipTeste = new[] { "unzip", "teste.gz", "extracted" };

            //args = zipTeste;

            if (args.Length == 0)
            {
                Console.WriteLine("Usage:");
                Console.WriteLine("GzipConsole zip [directory] [destFilename]");
                Console.WriteLine("GzipConsole unzip [sourceFilename] [directory]");
            }
            else
            {
                switch (args[0])
                {
                    case "zip":
                        {
                            TryZip(args.Skip(1).ToArray());
                            break;
                        }
                    case "unzip":
                        {
                            TryUnzip(args.Skip(1).ToArray());
                            break;
                        }
                }
            }
            Console.WriteLine("Pressione qualquer tecla para encerrar...");
            Console.ReadLine();
        }

        private static void TryZip(IList<string> args)
        {
            if (args.Count == 2)
            {
                string srcDir = args[0];
                string output = args[1];

                if (File.Exists(output))
                {
                    Console.WriteLine("arquivo de destino já existe. Informe outro nome.");
                    return;
                }

                Compression.CompressDirectory(srcDir, output, Console.WriteLine);

                Console.WriteLine("Arquivo {0} criado com sucesso.", output);
            }
            else
            {
                Console.WriteLine("Parametros inválidos");
            }
        }

        private static void TryUnzip(IList<string> args)
        {
            if (args.Count > 1)
            {


                var file = args[0];
                string destDir = AppDomain.CurrentDomain.BaseDirectory;
                if (args.Count == 2)
                {
                    destDir = args[1];
                }

                Compression.DecompressToDirectory(file, destDir, Console.WriteLine);

                Console.WriteLine("Arquivo {0} extraído com sucesso.", file);
            }
            else
            {
                Console.WriteLine("Parametros inválidos");
            }
        }
    }
}
